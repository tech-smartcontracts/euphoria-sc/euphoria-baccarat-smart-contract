// SPDX-License-Identifier: IRC
pragma solidity ^0.8.0;

import "@openzeppelin/contracts/token/ERC20/ERC20.sol";

contract EuphoriaToken is ERC20 {
    constructor () ERC20("Euphoria Coin", "EUPC") {
        _mint(msg.sender, 1000000000 * (10**uint256(decimals())));
    }

    function decimals() public view virtual override returns (uint8) {
        return 6;
    }
}