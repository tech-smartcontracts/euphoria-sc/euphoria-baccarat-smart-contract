// SPDX-License-Identifier: IRC
pragma solidity ^0.8.7;

import "./RandomNumberConsumer.sol";
import "@openzeppelin/contracts/token/ERC20/IERC20.sol";

contract GameManager {
    event requestIdCreated (bytes32 requestId);
    event gamePlayed (uint256 gameId);

    struct Card {
        string number;
        uint value;
        string suit;
    }

    struct Participant {
        address addr;
        uint256 bet;
        string name;
        Card[] hand;
        uint totalValue;
    }

    struct Game {
        uint256 id;
        uint256 randomNumber;
        Participant player;
        Participant dealer;
        Participant winner;
        Card[3] allPlayerCards;
        Card[3] allDealerCards;
        bool isCreated;
    }

    string[] internal CARDS = ['A', '2', '3', '4', '5', '6', '7', '8', '9', '10', 'J', 'Q', 'K'];
    uint[] internal VALUE = [1, 2, 3, 4, 5, 6, 7, 8, 9, 0, 0, 0, 0];
    string[] internal OUTCOME = ['PLAYER', 'DEALER', 'TIE'];
    string[] internal SUITS = ['c', 'd', 'h', 's'];

    // mapping (bytes32 => Game) public games;
    mapping (uint256 => Game) public games;
    uint256 private gameCount = 0;

    RandomNumberConsumer public randomNumberConsumer = RandomNumberConsumer(0xc599F7bdb3412799EF86A54954d63A9E2a91eFBd);
    IERC20 public token = IERC20(0xD132448aa448B8aB678Cc038350a45b3Ad9d750b);

    function updateRandomNumber() external returns (bytes32) {
        bytes32 requestId = randomNumberConsumer.getRandomNumber();
        emit requestIdCreated(requestId);
        return requestId;
    }

    function start(bytes32 requestId, address playerAddress, uint256 playerBet, address dealerAddress, uint256 dealerBet, uint commisson_rate) external {
        Game storage game = createGame(requestId);

        addPlayerToGame(game.id, playerAddress, playerBet);
        addDealerToGame(game.id, dealerAddress, dealerBet);

        dealCardsIn(game);
        play(game);
        determineWinnerOf(game);
        payToWinnerWithCommission(game, commisson_rate);

        emit gamePlayed(game.id);
    }

    function getRandomNumber(bytes32 requestId) public view returns (uint256) {
        return randomNumberConsumer.requestIdToRandomNumber(requestId);
    }

    function createGame(bytes32 requestId) internal returns (Game storage) {
        uint256 randomNumber = randomNumberConsumer.requestIdToRandomNumber(requestId);
        gameCount += 1;

        Game storage game = games[gameCount];

        game.id = gameCount;
        game.randomNumber = randomNumber;

        game.player.name = "PLAYER";
        game.player.totalValue = 0;

        game.dealer.name = "DEALER";
        game.dealer.totalValue = 0;

        game.isCreated = true;
        
        return game;
    }

    function addPlayerToGame(uint256 gameId, address addr, uint256 bet) internal {
        Game storage game = games[gameId];
        require(game.isCreated, "Game with given requestId doesn't exist");
        require(game.player.addr == address(0), "player already declared");
        require(token.allowance(addr, address(this))  >= bet, "Contract has not allowed to transfer token from given address");
        
        token.transferFrom(addr, address(this), bet);

        game.player.addr = addr;
        game.player.bet = bet;
    }

    function addDealerToGame(uint256 gameId, address addr, uint256 bet) internal {
        Game storage game = games[gameId];
        require(game.isCreated, "Game with given requestId doesn't exist");
        require(game.dealer.addr == address(0), "dealer already declared");
        require(token.allowance(addr, address(this)) >= bet, "Contract has not allowed to transfer token from given address");

        token.transferFrom(addr, address(this), bet);

        game.dealer.addr = addr;
        game.dealer.bet = bet;
    }

    function play(Game storage game) internal {
        require(areCardsDealtIn(game), "Cards in game are not dealt");

        game.player.totalValue = computeScore(game.player.hand);
        game.dealer.totalValue = computeScore(game.dealer.hand);

        if (inRange(game.player.totalValue, 8, 9) || inRange(game.dealer.totalValue, 8, 9)) {
            return;
        }

        // dealing new cards to participants and recomputing their scores
        if (inRange(game.player.totalValue, 0, 5)) {
            // now player has 3 cards
            game.player.hand.push(game.allPlayerCards[2]);

            uint playerThirdCardValue = game.player.hand[2].value;
            game.player.totalValue = computeScore(game.player.hand);

            bool conditionOne = (game.dealer.totalValue==6 && inRange(playerThirdCardValue, 6, 7));
            bool conditionTwo = (game.dealer.totalValue==5 && inRange(playerThirdCardValue, 4, 7));
            bool conditionThree = (game.dealer.totalValue==4 && inRange(playerThirdCardValue, 2, 7));
            bool conditionFour = (game.dealer.totalValue==3 && playerThirdCardValue!=8);
            bool conditionFive = inRange(game.dealer.totalValue, 0, 2);
            
            // determine if dealer needs a third card
            if (conditionOne || conditionTwo || conditionThree || conditionFour || conditionFive) {
                // now dealer has 3 cards
                game.dealer.hand.push(game.allDealerCards[2]);

                game.dealer.totalValue = computeScore(game.dealer.hand);
            }
        } else if (inRange(game.player.totalValue, 6, 7)) {
            if (inRange(game.dealer.totalValue, 0, 5)) {
                // now dealer has 3 cards
                game.dealer.hand.push(game.allDealerCards[2]);

                game.dealer.totalValue = computeScore(game.dealer.hand);
            }
        }
    }

    function payToWinnerWithCommission(Game storage game, uint commission_rate) internal {
        uint256 sum = game.player.bet + game.dealer.bet;
        uint256 commission = sum * commission_rate / 100;
        uint256 amountToPay = sum - commission;
        if (game.winner.addr == address(0)){
            transferTokenTo(game.dealer.addr, game.dealer.bet);
            transferTokenTo(game.player.addr, game.player.bet);
        } else {
            transferTokenTo(game.winner.addr, amountToPay);
        }
    }

    function transferTokenTo(address recipient, uint amount) internal{
        token.transfer(recipient, amount);
    }

    function dealCardsIn(Game storage game) internal {
        uint256[] memory expandedRandomResult = randomNumberConsumer.expand(game.randomNumber, 12);

        for (uint i; i < 3; i++) {
            string memory playerCardNumber = CARDS[expandedRandomResult[i] % 13];
            string memory playerCardSuit = SUITS[expandedRandomResult[i+3] % 4];
            uint playerCardValue = VALUE[cardIndex(playerCardNumber)];

            string memory dealerCardNumber = CARDS[expandedRandomResult[i+6] % 13];
            string memory dealerCardSuit = SUITS[expandedRandomResult[i+9] % 4];
            uint dealerCardValue = VALUE[cardIndex(dealerCardNumber)];

            game.allPlayerCards[i] = Card(playerCardNumber, playerCardValue , playerCardSuit);
            game.allDealerCards[i] = Card(dealerCardNumber, dealerCardValue, dealerCardSuit);
        }

        game.player.hand.push(game.allPlayerCards[0]);
        game.player.hand.push(game.allPlayerCards[1]);

        game.dealer.hand.push(game.allDealerCards[0]);
        game.dealer.hand.push(game.allDealerCards[1]);
    }

    function determineWinnerOf(Game storage game) internal returns(string memory) {
        if (game.player.totalValue > game.dealer.totalValue) {
            game.winner = game.player;
        } else if (game.player.totalValue < game.dealer.totalValue) {
            game.winner = game.dealer;
        } 
        return game.winner.name;
    }

    function computeScore(Card[] memory hand) internal pure returns (uint){
        uint totalValue = 0;
        for (uint i=0; i < hand.length; i++) {
            totalValue += hand[i].value;
        }

        return totalValue % 10;
    }

    function areCardsDealtIn(Game storage game) internal view returns (bool) {
        if (game.player.hand.length == 0 || game.dealer.hand.length == 0) return false;
        
        return true;
    }

    function cardIndex(string memory card) internal view returns (uint) {
        for (uint i=0; i < CARDS.length; i++) {
            string memory stored_card = CARDS[i];
            if (keccak256(abi.encodePacked(card)) == keccak256(abi.encodePacked(stored_card))) {
                return i;
            }
        }

        revert("card doesn't exist in CARDS array");
    }

    function inRange(uint number, uint startIndex, uint endIndex) internal pure returns (bool) {
        for (uint i=startIndex; i < endIndex+1; i++) {
            if (number == i) {
                return true;
            }
        }

        return false;
    }

    function getPlayer(uint256 gameId) external view returns (Participant memory) {
        Game memory game = games[gameId];
        require (game.isCreated, "Game with given requestId doesn't exist");

        return game.player;
    }

    function getDealer(uint256 gameId) external view returns (Participant memory) {
        Game memory game = games[gameId];
        require (game.isCreated, "Game with given requestId doesn't exist");

        return game.dealer;
    }

    function getParticipantHand(uint256 gameId, string memory name) external view returns(Card[] memory) {
        Game memory game = games[gameId];
        require (game.isCreated, "Game with given requestId doesn't exist");

        if (keccak256(abi.encodePacked(name)) == keccak256(abi.encodePacked(game.player.name))) {
            Card[] memory hand = game.player.hand;

            return hand;
        }
        else if (keccak256(abi.encodePacked(name)) == keccak256(abi.encodePacked(game.dealer.name))) {
            Card[] memory hand = game.dealer.hand;
            
            return hand;
        }
        else {
            revert("participant with given name doesn't exist");
        }
    }

    function getWinner(uint256 gameId) external view returns (string memory) {
        Game memory game = games[gameId];
        require (game.isCreated, "Game with given requestId doesn't exist");

        return game.winner.name;
    }
}