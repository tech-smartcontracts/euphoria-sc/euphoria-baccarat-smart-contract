import json
import os
from web3 import Web3
from dotenv import load_dotenv

basedir = os.path.abspath(os.path.dirname(__file__))
load_dotenv(os.path.join(basedir, '../.env'))

class Web3Service:
    def __init__(self) -> None:
        self.w3 = Web3(Web3.HTTPProvider("https://data-seed-prebsc-1-s1.binance.org:8545"))
        self.w3.eth.defaultAccount = '0xc94eF698724936e1ABcD0a284620ca47227EdBfd'

        self.bnb_chain_id = 97
        
        self.random_contract_ABI = json.loads('[{"inputs":[],"stateMutability":"nonpayable","type":"constructor"},{"anonymous":false,"inputs":[{"indexed":false,"internalType":"bytes32","name":"requestId","type":"bytes32"}],"name":"onGenerateRandomNumber","type":"event"},{"anonymous":false,"inputs":[{"indexed":false,"internalType":"uint256","name":"randomNumber","type":"uint256"}],"name":"randomNumberGenerated","type":"event"},{"inputs":[],"name":"randomNumber","outputs":[{"internalType":"uint256","name":"","type":"uint256"}],"stateMutability":"view","type":"function"},{"inputs":[{"internalType":"bytes32","name":"requestId","type":"bytes32"},{"internalType":"uint256","name":"randomness","type":"uint256"}],"name":"rawFulfillRandomness","outputs":[],"stateMutability":"nonpayable","type":"function"},{"inputs":[],"name":"getRandomNumber","outputs":[{"internalType":"bytes32","name":"requestId","type":"bytes32"}],"stateMutability":"nonpayable","type":"function"},{"inputs":[{"internalType":"uint256","name":"randomValue","type":"uint256"},{"internalType":"uint256","name":"n","type":"uint256"}],"name":"expand","outputs":[{"internalType":"uint256[]","name":"expandedValues","type":"uint256[]"}],"stateMutability":"pure","type":"function"}]')
        self.random_contract_address = '0xFEE02190D62cd940E2f602a6e3A3B472d94020C6'

        self.random_contract = self.w3.eth.contract(address=self.random_contract_address, abi=self.random_contract_ABI)
    

    def updateRandomNumber(self):
        tx = self.random_contract.functions.getRandomNumber().buildTransaction(
            {
                'nonce': self.w3.eth.get_transaction_count(self.w3.eth.defaultAccount),
                'from': self.w3.eth.defaultAccount,
                'chainId': self.bnb_chain_id
            }
        )

        signed_tx = self.w3.eth.account.sign_transaction(tx, os.getenv('MAIN_ACCOUNT_PRIVATE_KEY'))
        tx_hash = self.w3.eth.sendRawTransaction(signed_tx.rawTransaction)
        print(f'update random number tx hash: {tx_hash.hex()}')
        tx_receipt = self.w3.eth.wait_for_transaction_receipt('0x3a1940b4f2b15118e2ed5c7d9c11069a2fc8576cdd668d2759002a4523641fd8')
        rich_logs = self.random_contract.events.onGenerateRandomNumber().processReceipt(tx_receipt)
        # rich_logs = self.random_contract.events.randomNumberGenerated().processReceipt(tx_receipt)
        print(rich_logs[0]['args'])
        
        # return tx_hash.hex()

    def getRandomNumber(self):
        randomNumber = self.random_contract.functions.randomNumber().call()
        print(randomNumber)

        return randomNumber


ws3 = Web3Service()
ws3.updateRandomNumber()
ws3.getRandomNumber()