import time
import json
import os
from web3 import Web3
from dotenv import load_dotenv
import asyncio

basedir = os.path.abspath(os.path.dirname(__file__))
load_dotenv(os.path.join(basedir, '../.env'))

class Web3Service:
    def __init__(self) -> None:
        self.w3 = Web3(Web3.HTTPProvider("https://data-seed-prebsc-1-s1.binance.org:8545"))
        self.w3.eth.defaultAccount = '0xc94eF698724936e1ABcD0a284620ca47227EdBfd'

        self.bnb_chain_id = 97

        self.baccara_contract_ABI = json.loads('[{"inputs":[],"stateMutability":"nonpayable","type":"constructor"},{"anonymous":false,"inputs":[{"indexed":false,"internalType":"bytes32","name":"requestId","type":"bytes32"}],"name":"requestIdCreated","type":"event"},{"inputs":[],"name":"dealer","outputs":[{"internalType":"string","name":"name","type":"string"},{"internalType":"uint256","name":"totalValue","type":"uint256"}],"stateMutability":"view","type":"function","constant":true},{"inputs":[],"name":"player","outputs":[{"internalType":"string","name":"name","type":"string"},{"internalType":"uint256","name":"totalValue","type":"uint256"}],"stateMutability":"view","type":"function","constant":true},{"inputs":[],"name":"randomNumberConsumer","outputs":[{"internalType":"contractRandomNumberConsumer","name":"","type":"address"}],"stateMutability":"view","type":"function","constant":true},{"inputs":[],"name":"winner","outputs":[{"internalType":"string","name":"","type":"string"}],"stateMutability":"view","type":"function","constant":true},{"inputs":[{"internalType":"bytes32","name":"requestId","type":"bytes32"}],"name":"play","outputs":[],"stateMutability":"nonpayable","type":"function"},{"inputs":[],"name":"updateRandomNumber","outputs":[{"internalType":"bytes32","name":"","type":"bytes32"}],"stateMutability":"nonpayable","type":"function"},{"inputs":[{"internalType":"bytes32","name":"requestId","type":"bytes32"}],"name":"getRandomNumberByRequestId","outputs":[{"internalType":"uint256","name":"","type":"uint256"}],"stateMutability":"view","type":"function","constant":true},{"inputs":[{"internalType":"bytes32","name":"requestId","type":"bytes32"}],"name":"getRandomResult","outputs":[{"internalType":"uint256","name":"","type":"uint256"}],"stateMutability":"view","type":"function","constant":true},{"inputs":[{"internalType":"string","name":"name","type":"string"}],"name":"getParticipantHand","outputs":[{"components":[{"internalType":"string","name":"number","type":"string"},{"internalType":"uint256","name":"value","type":"uint256"},{"internalType":"string","name":"suit","type":"string"}],"internalType":"structBaccaraGame.Card[]","name":"","type":"tuple[]"}],"stateMutability":"view","type":"function","constant":true},{"inputs":[{"internalType":"string","name":"name","type":"string"}],"name":"getParticipantTotalValue","outputs":[{"internalType":"uint256","name":"","type":"uint256"}],"stateMutability":"view","type":"function","constant":true}]')

        self.game_manager_abi = json.loads('[{"anonymous":false,"inputs":[{"indexed":false,"internalType":"address","name":"newGame","type":"address"}],"name":"newGameCreated","type":"event"},{"anonymous":false,"inputs":[{"indexed":false,"internalType":"bytes32","name":"requestId","type":"bytes32"}],"name":"requestIdCreated","type":"event"},{"inputs":[{"internalType":"bytes32","name":"","type":"bytes32"}],"name":"games","outputs":[{"internalType":"address","name":"","type":"address"}],"stateMutability":"view","type":"function","constant":true},{"inputs":[],"name":"randomNumberConsumer","outputs":[{"internalType":"contractRandomNumberConsumer","name":"","type":"address"}],"stateMutability":"view","type":"function","constant":true},{"inputs":[],"name":"updateRandomNumber","outputs":[{"internalType":"bytes32","name":"","type":"bytes32"}],"stateMutability":"nonpayable","type":"function"},{"inputs":[{"internalType":"bytes32","name":"requestId","type":"bytes32"}],"name":"createGame","outputs":[{"internalType":"address","name":"","type":"address"}],"stateMutability":"nonpayable","type":"function"},{"inputs":[{"internalType":"bytes32","name":"requestId","type":"bytes32"}],"name":"getGameAddress","outputs":[{"internalType":"address","name":"","type":"address"}],"stateMutability":"view","type":"function","constant":true}]')
        self.game_manager_address = '0xa4bF2A6625a1bAA05615fC75c87cdA4D92F57b17'

        self.game_manager = self.w3.eth.contract(address=self.game_manager_address, abi=self.game_manager_abi)
    

    def updateRandomNumber(self):
        tx = self.game_manager.functions.updateRandomNumber().buildTransaction(
            {
                'nonce': self.w3.eth.get_transaction_count(self.w3.eth.defaultAccount),
                'from': self.w3.eth.defaultAccount,
                'chainId': self.bnb_chain_id
            }
        )

        signed_tx = self.w3.eth.account.sign_transaction(tx, os.getenv('MAIN_ACCOUNT_PRIVATE_KEY'))
        tx_hash = self.w3.eth.sendRawTransaction(signed_tx.rawTransaction)
        print(f'update random number tx hash: {tx_hash.hex()}')
        tx_receipt = self.w3.eth.wait_for_transaction_receipt(tx_hash)
        rich_logs = self.baccara_contract.events.requestIdCreated().processReceipt(tx_receipt)
        request_id = rich_logs[0]['args'].requestId
        
        return request_id

    
    def create_game(self, request_id):
        tx = self.game_manager.functions.createGame(request_id).buildTransaction(
            {
                'nonce': self.w3.eth.get_transaction_count(self.w3.eth.defaultAccount),
                'from': self.w3.eth.defaultAccount,
                'chainId': self.bnb_chain_id
            }
        )

        signed_tx = self.w3.eth.account.sign_transaction(tx, os.getenv('MAIN_ACCOUNT_PRIVATE_KEY'))
        tx_hash = self.w3.eth.sendRawTransaction(signed_tx.rawTransaction)
        print(f'update random number tx hash: {tx_hash.hex()}')
        tx_receipt = self.w3.eth.wait_for_transaction_receipt(tx_hash)
        rich_logs = self.game_manager.events.newGameCreated().processReceipt(tx_receipt)
        contract_address = rich_logs[0]['args'].newGame
        
        return contract_address
    

    def play(self, request_id, baccara_contract_address):

        baccara_contract = self.w3.eth.contract(address=baccara_contract_address, abi=self.baccara_contract_ABI)

        tx = baccara_contract.functions.play(request_id).buildTransaction(
            {
                'nonce': self.w3.eth.get_transaction_count(self.w3.eth.defaultAccount),
                'from': self.w3.eth.defaultAccount,
                'chainId': self.bnb_chain_id
            }
        )

        signed_tx = self.w3.eth.account.sign_transaction(tx, os.getenv('MAIN_ACCOUNT_PRIVATE_KEY'))
        tx_hash = self.w3.eth.sendRawTransaction(signed_tx.rawTransaction)
        print(f'play game tx hash: {tx_hash.hex()}')
        tx_receipt = self.w3.eth.wait_for_transaction_receipt(tx_hash)
        print(tx_receipt)
        
        return tx_hash.hex()    
    

    async def getRandomNumber(self, request_id, baccara_contract_address):
        baccara_contract = self.w3.eth.contract(address=baccara_contract_address, abi=self.baccara_contract_ABI)
        
        random_number = 0
        while random_number == 0:
            random_number = baccara_contract.functions.getRandomNumberByRequestId(request_id).call()
            print(random_number)
            await asyncio.sleep(25)

        return random_number
    

    def getWinner(self, baccara_contract_address):
        baccara_contract = self.w3.eth.contract(address=baccara_contract_address, abi=self.baccara_contract_ABI)

        winner = baccara_contract.functions.winner().call()
        print(winner)

        return winner


    def getPlayer(self, baccara_contract_address):
        baccara_contract = self.w3.eth.contract(address=baccara_contract_address, abi=self.baccara_contract_ABI)

        player = baccara_contract.functions.player().call()
        print(player)

        return player


    def getParticipantHand(self, name, baccara_contract_address):
        baccara_contract = self.w3.eth.contract(address=baccara_contract_address, abi=self.baccara_contract_ABI)

        participantHand = baccara_contract.functions.getParticipantHand(name).call()
        print(participantHand)

        return participantHand

    
    def getDealer(self, baccara_contract_address):
        baccara_contract = self.w3.eth.contract(address=baccara_contract_address, abi=self.baccara_contract_ABI)

        dealer = baccara_contract.functions.dealer().call()
        print(dealer)

        return dealer


async def main():
    ws3 = Web3Service()

    request_id = ws3.updateRandomNumber()
    baccara_address = ws3.create_game(request_id)
    await ws3.getRandomNumber(request_id, baccara_address)
    ws3.play(request_id, baccara_address)

    ws3.getPlayer(baccara_address)
    ws3.getDealer(baccara_address)

    ws3.getParticipantHand('player', baccara_address)
    ws3.getParticipantHand('dealer', baccara_address)

    ws3.getWinner(baccara_address)

asyncio.run(main())